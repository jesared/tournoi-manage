# Tournoi de Tennis - Backend

Bienvenue dans le backend de l'application Tournoi de Tennis. Cette application gère les participants et les utilisateurs pour un tournoi de tennis.

## Configuration

### 1. Installation des dépendances

Assurez-vous d'installer toutes les dépendances en exécutant la commande suivante :

### API Endpoints

Authentification Utilisateur

POST /api/users/register: Inscrivez un nouvel utilisateur.

{
  "firstName": "John",
  "lastName": "Doe",
  "email": "john.doe@example.com",
  "password": "password123"
}

POST /api/users/signin: Connectez-vous en tant qu'utilisateur.

{
  "email": "john.doe@example.com",
  "password": "password123"
}

POST /api/users/logout: Déconnectez-vous de l'application.