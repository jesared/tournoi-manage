require('dotenv').config();
const express = require('express');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const participantsRoutes = require('./routes/participants.route');
const usersRoutes = require('./routes/authUser.route');

const app = express();
const port = process.env.PORT || 3000; // Utilisez le port défini par l'environnement ou le port 3000 par défaut
const bddURL = process.env.BDD_URL;

// Ajoutez le middleware cookie-parser ici
app.use(cookieParser());

// Middleware pour gérer les données JSON
app.use(express.json());

// Middleware pour gérer les données de formulaire
app.use(express.urlencoded({ extended: true }));

// Connexion à la base de données MongoDB
mongoose.connect(bddURL)
  .then(() => {
    console.log('Connexion à la base de données MongoDB établie.');
  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données MongoDB:', error);
  });

// Utilisation des routes
app.use('/api/participants', participantsRoutes);
app.use('/api/users', usersRoutes);

// Port d'écoute
app.listen(port, () => {
  console.log(`Serveur Express en cours d'exécution sur le port ${port}`);
});
