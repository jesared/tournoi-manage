const mongoose = require('mongoose');

const participantSchema = new mongoose.Schema({
  licenseNumber: { type: String, required: true, unique: true },
  bibNumber: { type: Number, required: true, unique: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  phoneNumber: { type: String },
  points: { type: Number, required: true },
  registeredAt: { type: Date, default: Date.now },
  presenceDays: [{
    day: { type: Number },
    selected: { type: Boolean, default: false },
  }],
  participatingTabs: [{ type: String }],
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }, // Champ de référence à l'utilisateur
});


const Participant = mongoose.model('Participant', participantSchema);

module.exports = Participant;