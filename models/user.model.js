const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  firstName: { type: String, required: false },
  lastName: { type: String, required: false },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true, // Assure que l'e-mail est stocké en minuscules
    trim: true, // Supprime les espaces en début et fin de chaîne
  },
  password: { type: String, required: true }, // Ajoutez un champ pour stocker le mot de passe (vous devrez le hasher avant de le stocker)
  createdAt: { type: Date, default: Date.now }, // Ajoutez un champ pour enregistrer la date de création de l'utilisateur
  participants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Participant' }],
});

const User = mongoose.model('User', userSchema);

module.exports = User;
