const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

const router = express.Router();

// Inscription (Register)
router.post('/register', async (req, res) => {
  try {
    const { email, password } = req.body;

    // Vérifie si l'utilisateur existe déjà
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(400).json({ message: 'L\'utilisateur existe déjà.' });
    }

    // Hache le mot de passe
    const hashedPassword = await bcrypt.hash(password, 10);

    // Crée un nouvel utilisateur
    const user = new User({
      email,
      password: hashedPassword,
    });

    // Enregistre l'utilisateur dans la base de données
    await user.save();

    res.status(201).json({ message: 'Inscription réussie.' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Connexion (Sign In)
router.post('/signin', async (req, res) => {
    try {
      const { email, password } = req.body;
      const secretKeyJWT = process.env.SECRET_JWT;
  
      // Vérifie si l'utilisateur existe
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(401).json({ message: 'L\'utilisateur n\'existe pas.' });
      }
  
      // Vérifie le mot de passe
      const isPasswordValid = await bcrypt.compare(password, user.password);
      if (!isPasswordValid) {
        return res.status(401).json({ message: 'Mot de passe incorrect.' });
      }
  
      // Crée un jeton JWT
      const token = jwt.sign({ userId: user._id }, secretKeyJWT, { expiresIn: '1h' });
  
      // Définit le cookie avec le jeton JWT
      res.cookie('token', token, { httpOnly: true, maxAge: 3600000 }); // maxAge en millisecondes (1 heure dans cet exemple)
  
      res.json({ message: 'Connexion réussie.' });
  
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  });

// Déconnexion (Log Out)
router.post('/logout', (req, res) => {
  // Vous pouvez implémenter la déconnexion selon vos besoins
  res.clearCookie('token'); // Efface le cookie
  res.json({ message: 'Déconnexion réussie.' });
});

module.exports = router;
