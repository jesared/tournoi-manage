const express = require('express');
const cookieParser = require('cookie-parser');
const router = express.Router();
const Participant = require('../models/participants.model');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

// Ajoutez le middleware cookie-parser ici
router.use(cookieParser());

// Route pour créer un nouveau participant
router.post('/inscription', async (req, res) => {
  try {
    const token = req.cookies.token; // Récupérez le token depuis le cookie

    if (!token) {
      return res.status(401).json({ message: 'Authentification requise.' });
    }

    const secretKeyJWT = process.env.SECRET_JWT;

    // Vérifiez le token
    jwt.verify(token, secretKeyJWT, async (err, decoded) => {
      if (err) {
        return res.status(401).json({ message: 'Token invalide.' });
      }

      // Le token est valide, vous pouvez accéder à decoded.userId
      const user = await User.findById(decoded.userId);

      const participant = new Participant({
        licenseNumber: req.body.licenseNumber,
        bibNumber: req.body.bibNumber,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        points: req.body.points,
        user: user._id,
        // phoneNumber: req.body.phoneNumber,
        // presenceDays: req.body.presenceDays || [],
        // participatingTabs: req.body.participatingTabs || [],
      });

      if (!participant.bibNumber) {
        const Counter = mongoose.model('Counter', new mongoose.Schema({ count: Number }));
        const counter = await Counter.findOneAndUpdate({}, { $inc: { count: 1 } }, { upsert: true, new: true });
        participant.bibNumber = counter.count; // dossards
      }

      const newParticipant = await participant.save();

      // Mettez à jour la liste des participants de l'utilisateur
      user.participants.push(newParticipant._id);
      await user.save();

      res.status(201).json(newParticipant);
    });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

module.exports = router;
